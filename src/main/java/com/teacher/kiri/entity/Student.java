package com.teacher.kiri.entity;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="student")
public class Student {



    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="gender")
    private String gender;

    @Column(name="email")
    private String email;

    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="gcs_id")
    private GradeClassRoom gradeclassroom;

    public List<Assistance> getAssistances() {
        return assistances;
    }

    public void setAssistances(List<Assistance> assistances) {
        this.assistances = assistances;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public List<QualificationAverage> getQualificationAverages() {
        return qualificationAverages;
    }

    public void setQualificationAverages(List<QualificationAverage> qualificationAverages) {
        this.qualificationAverages = qualificationAverages;
    }

    @OneToMany(mappedBy="student",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<Assistance> assistances;

    @OneToMany(mappedBy="student",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<Qualification> qualifications;

    @OneToMany(mappedBy="student",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<QualificationAverage> qualificationAverages;


    public Student() {

    }

    public Student(String name, String gender, String email) {
        this.name = name;
        this.gender = gender;
        this.email = email;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public GradeClassRoom getGradeclassroom() {
        return gradeclassroom;
    }

    public void setGradeclassroom(GradeClassRoom gradeclassroom) {
        this.gradeclassroom = gradeclassroom;
    }

}
