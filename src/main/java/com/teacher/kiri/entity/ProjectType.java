package com.teacher.kiri.entity;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="project_type")
public class ProjectType {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;


    @Column(name="name")
    private String name;


    @Column(name="percentage")
    private String percentage;



    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="gcs_id")
    private GradeClassRoom gradeClassRoom;


    @OneToMany(mappedBy="projectType",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<Assistance> assistances;

    @OneToMany(mappedBy="projectType",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<Qualification> qualifications;

    @OneToMany(mappedBy="projectType",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<QualificationAverage> qualificationAverages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public List<Assistance> getAssistances() {
        return assistances;
    }

    public void setAssistances(List<Assistance> assistances) {
        this.assistances = assistances;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public List<QualificationAverage> getQualificationAverages() {
        return qualificationAverages;
    }

    public void setQualificationAverages(List<QualificationAverage> qualificationAverages) {
        this.qualificationAverages = qualificationAverages;
    }

    public GradeClassRoom getGradeClassRoom() {
        return gradeClassRoom;
    }

    public void setGradeClassRoom(GradeClassRoom gradeClassRoom) {
        this.gradeClassRoom = gradeClassRoom;
    }


}
