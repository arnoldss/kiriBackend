package com.teacher.kiri.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="administrator")
public class Administrator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="username")
    private String username;

    @Column(name="email")
    private String email;

    @Column(name="password")
    private String password;

    @Column(name="school_ids")
    private String school_ids;


    /*@OneToMany(mappedBy="administrator",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<GradeClassRoom> gradeClassRoomClassRoomSubjects;*/

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSchool() {
        return school_ids;
    }

    public void setSchool(String school_ids) {
        this.school_ids = school_ids;
    }

    public Administrator() {

    }

    public Administrator(String username, String email, String password, String school_ids) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.school_ids = school_ids;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", school=" + school_ids +
                '}';
    }




    // add convenience methods for bi-directional relationship

   /* public void add(GradeClassRoom tempGradeClassRoomClassRoomSubject) {

        if (gradeClassRoomClassRoomSubjects == null) {
            gradeClassRoomClassRoomSubjects = new ArrayList<>();
        }

        gradeClassRoomClassRoomSubjects.add(tempGradeClassRoomClassRoomSubject);

        tempGradeClassRoomClassRoomSubject.setAdministrator(this);
    }*/




}
