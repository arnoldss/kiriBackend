package com.teacher.kiri.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="school")
public class School {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="type")
    private String type;


    /*@JsonManagedReference
    @OneToMany(mappedBy="school",
            cascade= {CascadeType.PERSIST, CascadeType.MERGE,
                    CascadeType.DETACH, CascadeType.REFRESH})
    private List<Administrator> administrators;*/


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public School() {

    }

    public School(String name) {
        this.name = name;
    }

   /* public List<Administrator> getAdministrators() {
        return administrators;
    }

    public void setAdministrators(Administrator administrator) {
        this.administrators = administrators;
    }*/

    // add convenience methods for bi-directional relationship

   /* public void add(Administrator tempAdministrator) {

        if (administrators == null) {
            administrators = new ArrayList<>();
        }

        administrators.add(tempAdministrator);

        //tempAdministrator.setSchool(this);
    }*/

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "School{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
