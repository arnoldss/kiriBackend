package com.teacher.kiri.rest;

import com.teacher.kiri.dao.AdministratorRepository;
import com.teacher.kiri.entity.Administrator;
import com.teacher.kiri.entity.School;
import com.teacher.kiri.exception.ValidationException;
import com.teacher.kiri.security.JwtRequest;
import com.teacher.kiri.security.JwtToken;
import com.teacher.kiri.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;


@RestController()
public class AdministratorController {



    @Autowired
    AdministratorRepository administratorRepository;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtToken jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    EntityManager entityManager;




    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {


        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService

                .loadUserByUsername(authenticationRequest.getUsername());

        return ResponseEntity.ok(generateResponse(userDetails,authenticationRequest.getUsername()));

    }

    private Map<String, Object> generateResponse(UserDetails userDetails, String username) {
        final String token = jwtTokenUtil.generateToken(userDetails);
        Administrator user = administratorRepository.findByEmail(username);
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("role", user.getSchool());
        map.put("userId", user.getId());
        return map;
    }

    private void authenticate(String username, String password) throws Exception {

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

        } catch (DisabledException e) {

            throw new Exception("USER_DISABLED", e);

        } catch (BadCredentialsException e) {

            throw new Exception("INVALID_CREDENTIALS", e);

        }

    }






    @PostMapping("/createAdministrator")
    public Boolean create(@RequestBody Map<String, String> body) throws NoSuchAlgorithmException {
        String email = body.get("email");
        if (administratorRepository.existsByEmail(email)){

            throw new ValidationException("Username already existed");

        }

        //School school = new School();

       // school =  (School) entityManager.getReference(School.class, Integer.parseInt(body.get("school")));


        String password = body.get("password");
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
        String username = body.get("username");
        administratorRepository.save(new Administrator(username, email, encodedPassword, "" ));
        return true;
    }




  /*  private void convert(Map<String, String> body) {
        final ObjectMapper mapper = new ObjectMapper();
        Person person = mapper.convertValue(body, PerfindByEmailson.class);
        UserInfo user = userInfoRepository.findById(Integer.valueOf(body.get("userId"))).get();
        person.setUser(user);
        personServiceImpl.setStatus(person);
        personRepository.save(person);
    }   */


}
