package com.teacher.kiri.rest;

import com.teacher.kiri.dao.SchoolRepository;
import com.teacher.kiri.entity.Administrator;
import com.teacher.kiri.entity.School;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;


@RestController()
public class SchoolController {



    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    EntityManager entityManager;




    @GetMapping(value = "/schoolsByStateAndType")
    public ResponseEntity<?> getSomeResourceWithParameters(/*@RequestParam String id,*/
     @RequestParam("state") String state,  @RequestParam("type") String type) {

        School[] schools = schoolRepository.findByType(type);
        /*if(schools.length >=0) {
            for (int i = 0; i < schools.length; i++)
            {
                List<Administrator> a
                        = schools[i].getAdministrators();
                for (int j = 0; j < a.size(); j++) {
                    a.set(j, null);
                    schools[i].setAdministrators(a.get(j));
                }

            }
            schools.stream().forEach((p)-> {
                System.out.println(p.getNombre());
                System.out.println(p.getApellidos());
                System.out.println(p.getEdad());
            });
        } */
        return new ResponseEntity<Object>(schools, HttpStatus.OK);
    }


    @GetMapping(value = "/getLatestSchool")
    public ResponseEntity<?> getLatestSchool() {

        School schools = schoolRepository.findTopByOrderByIdDesc();

        return new ResponseEntity<Object>(schools, HttpStatus.OK);
    }

    @PostMapping("/createSchool")
    public int create(@RequestBody Map<String, String> body) throws NoSuchAlgorithmException {
        String email = body.get("email");
       /* if (administratorRepository.existsByEmail(email)){

            throw new ValidationException("Username already existed");

        }*/

        School school = new School();
        school.setName(body.get("name"));
        //school.setAdministrators(body.get("administrator"));
        school.setType(body.get("type"));

        return schoolRepository.save(school).getId();
    }





}
