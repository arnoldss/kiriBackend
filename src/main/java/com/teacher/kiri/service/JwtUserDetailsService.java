package com.teacher.kiri.service;

import com.teacher.kiri.dao.AdministratorRepository;
import com.teacher.kiri.entity.Administrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private AdministratorRepository administratorRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Administrator administrator = administratorRepository.findByEmail(email);
        if (administrator == null) {
            throw new UsernameNotFoundException("User not found with email: " + email);
        }
        return new org.springframework.security.core.userdetails.User(administrator.getEmail(), administrator.getPassword(),
                new ArrayList<>());
    }
}