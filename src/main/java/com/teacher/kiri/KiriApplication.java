package com.teacher.kiri;

import com.teacher.kiri.entity.GradeClassRoom;
import com.teacher.kiri.entity.School;
import com.teacher.kiri.entity.Student;
import com.teacher.kiri.security.JwtAuthenticationEntryPoint;
import com.teacher.kiri.security.JwtRequestFilter;
import com.teacher.kiri.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import java.util.Arrays;

import org.springframework.context.annotation.Configuration;

import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;


@SpringBootApplication
public class KiriApplication {



	public static void main(String[] args) {
		SpringApplication.run(KiriApplication.class,				args);
	}

	@Configuration
	public class RepositoryConfiguration extends RepositoryRestConfigurerAdapter  {

		@Override
		public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
			config.exposeIdsFor(Student.class);
			config.exposeIdsFor(School.class);
			config.exposeIdsFor(GradeClassRoom.class);


		}

	}



	@Configuration
	@EnableWebSecurity
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	public class SecurityConfig extends WebSecurityConfigurerAdapter {

		@Autowired
		private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

		@Autowired
		private JwtUserDetailsService jwtUserDetailsService;

		@Autowired
		private JwtRequestFilter jwtRequestFilter;

		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

			auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());

		}

		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder();
		}

		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();

		}


		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {

			httpSecurity.csrf().disable().cors().and()
					// dont authenticate this particular request
					.authorizeRequests().antMatchers("/createAdministrator",
					"/authenticate",
					"/schoolsByType",
					"/createSchool",

							"/schools").permitAll().
					// all other requests need to be authenticated
							anyRequest().authenticated().and().
					// make sure we use stateless session; session won't be used to
					// store user's state.
							exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			// Add a filter to validate the tokens with every request
			httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);


		}




		@Bean
		CorsConfigurationSource corsConfigurationSource() {
			CorsConfiguration configuration = new CorsConfiguration();
			configuration.setAllowedOrigins(Arrays.asList("*"));
			configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
			configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
			configuration.setExposedHeaders(Arrays.asList("x-auth-token"));
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			source.registerCorsConfiguration("/**", configuration);
			return source;
		}
		}



	}















