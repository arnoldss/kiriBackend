package com.teacher.kiri.dao;


import com.teacher.kiri.entity.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolRepository extends JpaRepository <School,Integer> {


 School[] findByType(String type);


 School findTopByOrderByIdDesc();



}
