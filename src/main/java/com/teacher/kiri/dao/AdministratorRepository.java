package com.teacher.kiri.dao;


import com.teacher.kiri.entity.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository <Administrator,Integer>//, AdministratorCustomRepository
{

    Administrator findByEmail(String email);
    Boolean existsByEmail(String email);





}


