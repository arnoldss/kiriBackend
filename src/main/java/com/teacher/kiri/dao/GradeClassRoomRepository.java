package com.teacher.kiri.dao;


import com.teacher.kiri.entity.GradeClassRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeClassRoomRepository extends JpaRepository<GradeClassRoom,Integer> {





}
