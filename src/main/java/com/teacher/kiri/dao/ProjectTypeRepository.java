package com.teacher.kiri.dao;


import com.teacher.kiri.entity.ProjectType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectTypeRepository extends JpaRepository <ProjectType,Integer> {





}
