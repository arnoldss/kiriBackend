package com.teacher.kiri.dao;


import com.teacher.kiri.entity.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QualificationRepository extends JpaRepository <Qualification,Integer> {





}
